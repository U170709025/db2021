use company;

create temporary table TopSuppliers(
    SupplierID int primary key,
    SupplierName varchar(45),
    ProductCount int
);
select suppliers.SupplierID , SupplierName , count(ProductID) as ProductCount
from suppliers join products on suppliers.SupplierID = products.supplierID
group by SupplierName
order by ProductCount desc;

select * from TopSuppliers where ProductCount > 3  order by ProductCount desc;

call check_table_exists('TopSuppliers');
drop temporary table  TopSuppliers;

select * from customers;

update customers 
set CustomerName = "Dilara Sahan" , City = "Osmaniye", PostalCode = "80010", Country = "Turkey"
where CustomerID = 1;

delete from customers where CustomerID = 1;

truncate table Customers;
delete from Customers;
drop table Customers;
